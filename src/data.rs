#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Dish {
    pub name: String,
    pub source: Option<String>,
    pub elements: Vec<DishIngredient>,
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Ingredient {
    pub name: String,
    pub unit: String,
    pub category: Category,
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct DishIngredient {
    pub ingredient: Ingredient,
    pub quantity: Option<usize>,
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum Category {
    Légumes,
    Fruits,
    Frais,
    Viandes,
    FromagesPain,
    Conserves,
    Monde,
    Féculents,
    Déjeuner,
    CrêmeLaitOeufs,
    AlcoolApéritif,
    MénageEntretienPQ,
    Chats,
    Bio,
    Surgelé,
    Autre,
}

#[allow(unused_variables)]
pub fn build_cook_book() -> Vec<Dish> {
    // Ingredients - Légumes
    let ail = Ingredient {
        name: "Gousse d'aîl".to_string(),
        unit: "u".to_string(),
        category: Category::Légumes,
    };

    let noix = Ingredient {
        name: "Noix".to_string(),
        unit: " g".to_string(),
        category: Category::Légumes,
    };

    let amandes = Ingredient {
        name: "Amandes".to_string(),
        unit: " poignée(s)".to_string(),
        category: Category::Légumes,
    };

    let tomate = Ingredient {
        name: "Tomate(s)".to_string(),
        unit: "u".to_string(),
        category: Category::Légumes,
    };

    let tomates_cerises = Ingredient {
        name: "Tomates cerises".to_string(),
        unit: " g".to_string(),
        category: Category::Légumes,
    };

    let avocat = Ingredient {
        name: "Avocat(s)".to_string(),
        unit: "u".to_string(),
        category: Category::Légumes,
    };

    let courgette_fraiche = Ingredient {
        name: "Courgette(s) fraiche(s)".to_string(),
        unit: "u".to_string(),
        category: Category::Légumes,
    };

    let piment_rouge = Ingredient {
        name: "Piment(s) rouge(s)".to_string(),
        unit: "u".to_string(),
        category: Category::Légumes,
    };

    let carotte_fraiche = Ingredient {
        name: "Carotte(s) fraiche(s)".to_string(),
        unit: "u".to_string(),
        category: Category::Légumes,
    };

    let chou_fleur_frais = Ingredient {
        name: "Choux fleur frais".to_string(),
        unit: "u".to_string(),
        category: Category::Légumes,
    };

    let brocolis = Ingredient {
        name: "Brocolis frais".to_string(),
        unit: "g".to_string(),
        category: Category::Légumes,
    };

    let onions = Ingredient {
        name: "Onions".to_string(),
        unit: "u".to_string(),
        category: Category::Légumes,
    };

    let champignons_frais = Ingredient {
        name: "Champignons frais".to_string(),
        unit: "g".to_string(),
        category: Category::Légumes,
    };

    let mache = Ingredient {
        name: "Mâche".to_string(),
        unit: " poignée(s)".to_string(),
        category: Category::Légumes,
    };

    let sucrine = Ingredient {
        name: "Sucrine".to_string(),
        unit: "u".to_string(),
        category: Category::Légumes,
    };

    let salade = Ingredient {
        name: "Salade".to_string(),
        unit: "u".to_string(),
        category: Category::Légumes,
    };

    let radis = Ingredient {
        name: "Radis".to_string(),
        unit: " poignée(s)".to_string(),
        category: Category::Légumes,
    };

    let pommes_de_terre = Ingredient {
        name: "Pommes de terre".to_string(),
        unit: "u".to_string(),
        category: Category::Légumes,
    };

    let patate_douce = Ingredient {
        name: "Patate douce".to_string(),
        unit: "u".to_string(),
        category: Category::Légumes,
    };

    let echalottes = Ingredient {
        name: "Échalottes".to_string(),
        unit: "u".to_string(),
        category: Category::Légumes,
    };

    let celeri = Ingredient {
        name: "Céleri (pousse)".to_string(),
        unit: "u".to_string(),
        category: Category::Légumes,
    };

    let aubergines = Ingredient {
        name: "Aubergines".to_string(),
        unit: "u".to_string(),
        category: Category::Légumes,
    };

    // Ingrédients - Fruits

    let pomme = Ingredient {
        name: "Pomme(s)".to_string(),
        unit: "u".to_string(),
        category: Category::Fruits,
    };

    let poire = Ingredient {
        name: "Poire(s)".to_string(),
        unit: "u".to_string(),
        category: Category::Fruits,
    };

    let orange = Ingredient {
        name: "Orange(s)".to_string(),
        unit: "u".to_string(),
        category: Category::Fruits,
    };

    let figues = Ingredient {
        name: "Figue(s)".to_string(),
        unit: "poignée(s)".to_string(),
        category: Category::Légumes,
    };

    let melon = Ingredient {
        name: "Melon(s)".to_string(),
        unit: "u".to_string(),
        category: Category::Fruits,
    };

    let pasteque = Ingredient {
        name: "Pasteque(s)".to_string(),
        unit: "u".to_string(),
        category: Category::Fruits,
    };

    let abricot = Ingredient {
        name: "Abricot(s)".to_string(),
        unit: "u".to_string(),
        category: Category::Fruits,
    };

    let cerise = Ingredient {
        name: "Cerise(s)".to_string(),
        unit: "u".to_string(),
        category: Category::Fruits,
    };

    let mandarine = Ingredient {
        name: "Mandarine(s)".to_string(),
        unit: "u".to_string(),
        category: Category::Fruits,
    };

    // Ingrédients - Frais

    let jambon_blanc = Ingredient {
        name: "Jambon blanc (en tranche)".to_string(),
        unit: "u".to_string(),
        category: Category::Frais,
    };

    let jambon_cru = Ingredient {
        name: "Jambon cru (en tranche)".to_string(),
        unit: "u".to_string(),
        category: Category::Frais,
    };

    let lardons = Ingredient {
        name: "Lardons (en barquettes)".to_string(),
        unit: "u".to_string(),
        category: Category::Frais,
    };

    let salami = Ingredient {
        name: "Salami (en tranche)".to_string(),
        unit: "u".to_string(),
        category: Category::Frais,
    };

    let saucisse_knacki = Ingredient {
        name: "Saucisse type knacki".to_string(),
        unit: "u".to_string(),
        category: Category::Frais,
    };

    let raviolis = Ingredient {
        name: "Raviolis".to_string(),
        unit: "g".to_string(),
        category: Category::Frais,
    };

    let ravioles = Ingredient {
        name: "Ravioles".to_string(),
        unit: "g".to_string(),
        category: Category::Frais,
    };

    let pate_a_pizza = Ingredient {
        name: "Pâte à pizza".to_string(),
        unit: "u".to_string(),
        category: Category::Frais,
    };

    let pate_brisee = Ingredient {
        name: "Pâte brisée".to_string(),
        unit: "u".to_string(),
        category: Category::Frais,
    };

    // Ingrédients - Viandes

    let filet_poulet = Ingredient {
        name: "Filet de poulet".to_string(),
        unit: "u".to_string(),
        category: Category::Viandes,
    };

    let steak_hache = Ingredient {
        name: "Steak haché".to_string(),
        unit: "u".to_string(),
        category: Category::Viandes,
    };

    let steak = Ingredient {
        name: "Steak".to_string(),
        unit: "u".to_string(),
        category: Category::Viandes,
    };

    let saumon_frais = Ingredient {
        name: "Saumon frais (pavé)".to_string(),
        unit: "u".to_string(),
        category: Category::Viandes,
    };

    // Ingrédients - Fromages / Pain

    let pain = Ingredient {
        name: "Miche(s) de pain".to_string(),
        unit: "u".to_string(),
        category: Category::FromagesPain,
    };

    let baguette = Ingredient {
        name: "Baguette(s)".to_string(),
        unit: "u".to_string(),
        category: Category::FromagesPain,
    };

    let emmental_rape = Ingredient {
        name: "Emmental rapé".to_string(),
        unit: "g".to_string(),
        category: Category::FromagesPain,
    };

    let emmental = Ingredient {
        name: "Emmental".to_string(),
        unit: "g".to_string(),
        category: Category::FromagesPain,
    };

    let parmesan = Ingredient {
        name: "Parmesan".to_string(),
        unit: "g".to_string(),
        category: Category::FromagesPain,
    };

    let comte = Ingredient {
        name: "Comté".to_string(),
        unit: "g".to_string(),
        category: Category::FromagesPain,
    };

    let bleu = Ingredient {
        name: "Bleu (Roquefort,Gorgonzola,etc)".to_string(),
        unit: "g".to_string(),
        category: Category::FromagesPain,
    };

    let feta = Ingredient {
        name: "Bloc de feta".to_string(),
        unit: "u".to_string(),
        category: Category::FromagesPain,
    };

    let mozzarella = Ingredient {
        name: "Mozzarella".to_string(),
        unit: "u".to_string(),
        category: Category::FromagesPain,
    };

    let chevre_frais = Ingredient {
        name: "Chèvre frais (pot)".to_string(),
        unit: "u".to_string(),
        category: Category::FromagesPain,
    };

    let chevre_buche = Ingredient {
        name: "Chèvre (en buche)".to_string(),
        unit: "u".to_string(),
        category: Category::FromagesPain,
    };

    let fromage_blanc_chevre = Ingredient {
        name: "Fromage blanc de chèvre".to_string(),
        unit: "g".to_string(),
        category: Category::FromagesPain,
    };

    let cancoillote = Ingredient {
        name: "Cancoillote (en pot)".to_string(),
        unit: "u".to_string(),
        category: Category::FromagesPain,
    };

    // Ingrédients - Conserves

    let thon = Ingredient {
        name: "Thon".to_string(),
        unit: "g".to_string(),
        category: Category::Conserves,
    };

    let mais = Ingredient {
        name: "Maïs".to_string(),
        unit: "g".to_string(),
        category: Category::Conserves,
    };

    let concentre_tomates = Ingredient {
        name: "Concentré de tomates".to_string(),
        unit: "g".to_string(),
        category: Category::Conserves,
    };

    let petits_pois = Ingredient {
        name: "Petis pois".to_string(),
        unit: "g".to_string(),
        category: Category::Conserves,
    };

    let haricots_rouges = Ingredient {
        name: "Haricots rouges".to_string(),
        unit: "g".to_string(),
        category: Category::Conserves,
    };

    let haricots_blancs = Ingredient {
        name: "Haricots blancs".to_string(),
        unit: "g".to_string(),
        category: Category::Conserves,
    };

    let tomates_sechees = Ingredient {
        name: "Tomates séchées".to_string(),
        unit: "u".to_string(),
        category: Category::Conserves,
    };

    let olives_noires = Ingredient {
        name: "Olives noires (poignées)".to_string(),
        unit: "u".to_string(),
        category: Category::Conserves,
    };

    let olives_vertes = Ingredient {
        name: "Olives vertes (poignées)".to_string(),
        unit: "u".to_string(),
        category: Category::Conserves,
    };

    let capres = Ingredient {
        name: "Capres".to_string(),
        unit: " g".to_string(),
        category: Category::Conserves,
    };

    // Ingrédients - Monde

    let riz = Ingredient {
        name: "Riz".to_string(),
        unit: "g".to_string(),
        category: Category::Monde,
    };

    let wraps = Ingredient {
        name: "Wraps".to_string(),
        unit: "u".to_string(),
        category: Category::Monde,
    };

    let houmous = Ingredient {
        name: "Houmous (pot)".to_string(),
        unit: "u".to_string(),
        category: Category::Monde,
    };

    let raisins_secs = Ingredient {
        name: "Raisins secs".to_string(),
        unit: " poignée(s)".to_string(),
        category: Category::Monde,
    };

    let beurre_cacahuetes = Ingredient {
        name: "Beurre de cacahuètes".to_string(),
        unit: " cuillères".to_string(),
        category: Category::Monde,
    };

    // Ingrédients - Féculents

    let pates = Ingredient {
        name: "Pates".to_string(),
        unit: "g".to_string(),
        category: Category::Féculents,
    };

    let spaghettis = Ingredient {
        name: "Spaghettis".to_string(),
        unit: "g".to_string(),
        category: Category::Féculents,
    };

    let boulghour = Ingredient {
        name: "Boulghour".to_string(),
        unit: "g".to_string(),
        category: Category::Féculents,
    };

    let chapelure = Ingredient {
        name: "Chapelure".to_string(),
        unit: "".to_string(),
        category: Category::Féculents,
    };

    let quinoa = Ingredient {
        name: "Boulghour".to_string(),
        unit: "g".to_string(),
        category: Category::Féculents,
    };

    let lentilles_vertes = Ingredient {
        name: "Lentilles vertes".to_string(),
        unit: "g".to_string(),
        category: Category::Féculents,
    };

    let lentilles_corail = Ingredient {
        name: "Lentilles corail".to_string(),
        unit: "g".to_string(),
        category: Category::Féculents,
    };

    let couscous = Ingredient {
        name: "Couscous".to_string(),
        unit: "g".to_string(),
        category: Category::Féculents,
    };

    let mayonnaise = Ingredient {
        name: "Mayonnaise".to_string(),
        unit: "g".to_string(),
        category: Category::Féculents,
    };

    let moutarde = Ingredient {
        name: "Moutarde".to_string(),
        unit: "g".to_string(),
        category: Category::Féculents,
    };

    let jus_de_citron = Ingredient {
        name: "Jus de citron".to_string(),
        unit: " g".to_string(),
        category: Category::Féculents,
    };

    let piment = Ingredient {
        name: "Piment (épice)".to_string(),
        unit: "g".to_string(),
        category: Category::Féculents,
    };

    let persil = Ingredient {
        name: "Persil (épice)".to_string(),
        unit: "g".to_string(),
        category: Category::Féculents,
    };

    let masala = Ingredient {
        name: "Masala (épice)".to_string(),
        unit: "g".to_string(),
        category: Category::Féculents,
    };

    let cumin = Ingredient {
        name: "Cumin (épice)".to_string(),
        unit: "g".to_string(),
        category: Category::Féculents,
    };

    let curcuma = Ingredient {
        name: "Curcuma (épice)".to_string(),
        unit: "g".to_string(),
        category: Category::Féculents,
    };

    let bouillon_legumes = Ingredient {
        name: "Bouillon de légumes".to_string(),
        unit: "".to_string(),
        category: Category::Féculents,
    };

    // Ingrédients - Déjeuner

    let brioche = Ingredient {
        name: "Brioche".to_string(),
        unit: "u".to_string(),
        category: Category::Déjeuner,
    };

    let pain_burger = Ingredient {
        name: "Pain à burgers".to_string(),
        unit: "u".to_string(),
        category: Category::Déjeuner,
    };

    let avoine = Ingredient {
        name: "Flocons d'avoine".to_string(),
        unit: "g".to_string(),
        category: Category::Déjeuner,
    };

    // Ingrédients - Crême / Lait / Oeufs

    let lait = Ingredient {
        name: "Lait".to_string(),
        unit: "mL".to_string(),
        category: Category::CrêmeLaitOeufs,
    };

    let oeuf = Ingredient {
        name: "Oeuf(s)".to_string(),
        unit: "u".to_string(),
        category: Category::CrêmeLaitOeufs,
    };

    let creme = Ingredient {
        name: "Crème liquide".to_string(),
        unit: "mL".to_string(),
        category: Category::CrêmeLaitOeufs,
    };

    let creme_soja = Ingredient {
        name: "Crème de soja liquide".to_string(),
        unit: "mL".to_string(),
        category: Category::CrêmeLaitOeufs,
    };

    let beurre = Ingredient {
        name: "Beurre".to_string(),
        unit: "g".to_string(),
        category: Category::CrêmeLaitOeufs,
    };

    // Ingrédients - Ménage / Entretien / PQ

    let pq = Ingredient {
        name: "PQ".to_string(),
        unit: "u".to_string(),
        category: Category::MénageEntretienPQ,
    };

    let sacs_poubelle = Ingredient {
        name: "Sacs poubelle (30L)".to_string(),
        unit: "u".to_string(),
        category: Category::MénageEntretienPQ,
    };

    // Ingrédients - Alcool / Apéritifs

    let cognac = Ingredient {
        name: "Cognac".to_string(),
        unit: " mL".to_string(),
        category: Category::AlcoolApéritif,
    };

    let cacahuetes = Ingredient {
        name: "Cacahuètes".to_string(),
        unit: " poignée(s)".to_string(),
        category: Category::AlcoolApéritif,
    };

    // Ingrédients - Chats

    let litiere = Ingredient {
        name: "Litière".to_string(),
        unit: "u".to_string(),
        category: Category::Chats,
    };

    let croquettes = Ingredient {
        name: "Croquettes (préférer prendre en animalerie)".to_string(),
        unit: "kg".to_string(),
        category: Category::Chats,
    };

    // Ingrédients - Bio

    let tofu = Ingredient {
        name: "Tofu (fumé)".to_string(),
        unit: "g".to_string(),
        category: Category::Bio,
    };

    let seitan = Ingredient {
        name: "Seitan".to_string(),
        unit: "g".to_string(),
        category: Category::Bio,
    };

    // Ingrédients - Surgelé

    let pommes_dauphines = Ingredient {
        name: "Pommes dauphines".to_string(),
        unit: " poignée(s)".to_string(),
        category: Category::Surgelé,
    };

    let courgettes_surgelee = Ingredient {
        name: "Courgettes (surgelées)".to_string(),
        unit: "g".to_string(),
        category: Category::Surgelé,
    };

    let chou_fleur_surgele = Ingredient {
        name: "Choux fleur (surgelées)".to_string(),
        unit: "g".to_string(),
        category: Category::Surgelé,
    };

    let epinards_surgele = Ingredient {
        name: "Épinards (surgelées)".to_string(),
        unit: "g".to_string(),
        category: Category::Surgelé,
    };

    let poireaux_surgeles = Ingredient {
        name: "Poireaux (surgelées)".to_string(),
        unit: "g".to_string(),
        category: Category::Surgelé,
    };

    // Ingrédients - Autre (emplacement inconnu)

    let huile_arachide = Ingredient {
        name: "Huile d'arachide".to_string(),
        unit: "mL".to_string(),
        category: Category::Autre,
    };

    // === Recettes ===
    let mut cook_book: Vec<Dish> = Vec::new();
    cook_book.push(Dish {
        name: "Pates au thon".to_string(),
        source: Some("Livre custom".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: riz.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: thon.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: mayonnaise.clone(),
                quantity: None,
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Bols de patate douce, houmous, brocoli et riz".to_string(),
        source: Some("Mes bols santé, poké bowls et bouddha bowls, page 23".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: riz.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: patate_douce.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: houmous.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: brocolis.clone(),
                quantity: Some(320),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Bols au poulet au curcuma, noix de cajou et quinoa".to_string(),
        source: Some("Mes bols santé, poké bowls et bouddha bowls, page 24".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: quinoa.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: filet_poulet.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: avocat.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: cacahuetes.clone(),
                quantity: None,
            },
            DishIngredient {
                ingredient: raisins_secs.clone(),
                quantity: None,
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Bols mâche, quinoa, feta, tomates cerises, aîl roti et olives".to_string(),
        source: Some("Mes bols santé, poké bowls et bouddha bowls, page 28".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: mache.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: quinoa.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: tomates_cerises.clone(),
                quantity: Some(500),
            },
            DishIngredient {
                ingredient: ail.clone(),
                quantity: Some(12),
            },
            DishIngredient {
                ingredient: feta.clone(),
                quantity: Some(1),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Bols de lentilles corail kinda spicy".to_string(),
        source: Some("Mes bols santé, poké bowls et bouddha bowls, page 41".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: sucrine.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: lentilles_corail.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: radis.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: couscous.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: pommes_dauphines.clone(),
                quantity: Some(4),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Bols saumon aux figues, noix et avocat".to_string(),
        source: Some("Mes bols santé, poké bowls et bouddha bowls, page 42".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: saumon_frais.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: avocat.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: figues.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: mache.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: raisins_secs.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: noix.clone(),
                quantity: None,
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Bols riz, avocat, tofu mariné, beurre de cacahuètes et maïs".to_string(),
        source: Some("Mes bols santé, poké bowls et bouddha bowls, page 45".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: tofu.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: avocat.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: riz.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: mais.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: beurre_cacahuetes.clone(),
                quantity: Some(4),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Bols de tofu, potiron, riz et chèvre frais".to_string(),
        source: Some("Mes bols santé, poké bowls et bouddha bowls, page 46".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: haricots_rouges.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: tofu.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: riz.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: mache.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: chevre_frais.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: raisins_secs.clone(),
                quantity: Some(4),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Gratin de chou-fleur".to_string(),
        source: Some("Mini gratins en 30 recettes, page 20".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: chou_fleur_surgele.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: beurre.clone(),
                quantity: Some(60),
            },
            DishIngredient {
                ingredient: lait.clone(),
                quantity: Some(1000),
            },
            DishIngredient {
                ingredient: emmental_rape.clone(),
                quantity: Some(200),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Gratin de pâtes".to_string(),
        source: Some("Mini gratins en 30 recettes, page 32".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: champignons_frais.clone(),
                quantity: Some(150),
            },
            DishIngredient {
                ingredient: saucisse_knacki.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: creme_soja.clone(),
                quantity: Some(20),
            },
            DishIngredient {
                ingredient: emmental_rape.clone(),
                quantity: Some(100),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Pizza chèvre miel".to_string(),
        source: Some("Livre perso".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: pate_a_pizza.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: chevre_frais.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: chevre_buche.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: mozzarella.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: noix.clone(),
                quantity: None,
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Bread pudding aux tomates".to_string(),
        source: Some("Livre perso".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: tomate.clone(),
                quantity: Some(6),
            },
            DishIngredient {
                ingredient: baguette.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: oeuf.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: emmental_rape.clone(),
                quantity: Some(50),
            },
            DishIngredient {
                ingredient: creme_soja.clone(),
                quantity: Some(150),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Flan au fromage blanc de chèvre aux épinards".to_string(),
        source: Some("Livre perso".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: fromage_blanc_chevre.clone(),
                quantity: Some(250),
            },
            DishIngredient {
                ingredient: oeuf.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: epinards_surgele.clone(),
                quantity: Some(50),
            },
            DishIngredient {
                ingredient: tomates_sechees.clone(),
                quantity: Some(150),
            },
            DishIngredient {
                ingredient: moutarde.clone(),
                quantity: None,
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Quiche lorraine au tofu".to_string(),
        source: Some("Livre perso".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: pate_brisee.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: oeuf.clone(),
                quantity: Some(3),
            },
            DishIngredient {
                ingredient: lait.clone(),
                quantity: Some(250),
            },
            DishIngredient {
                ingredient: creme_soja.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: comte.clone(),
                quantity: Some(100),
            },
            DishIngredient {
                ingredient: tofu.clone(),
                quantity: Some(200),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Crumble de courgettes".to_string(),
        source: Some("Livre perso".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: courgette_fraiche.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: emmental_rape.clone(),
                quantity: Some(15),
            },
            DishIngredient {
                ingredient: avoine.clone(),
                quantity: Some(50),
            },
            DishIngredient {
                ingredient: chevre_frais.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: moutarde.clone(),
                quantity: None,
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Riz aux lentilles corail et curry".to_string(),
        source: Some("Livre perso".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: riz.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: lentilles_corail.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: carotte_fraiche.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: courgette_fraiche.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: onions.clone(),
                quantity: None,
            },
            DishIngredient {
                ingredient: moutarde.clone(),
                quantity: None,
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Riz cantonnais".to_string(),
        source: Some("Livre perso".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: riz.clone(),
                quantity: Some(250),
            },
            DishIngredient {
                ingredient: oeuf.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: onions.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: carotte_fraiche.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: jambon_blanc.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: petits_pois.clone(),
                quantity: Some(80),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Salade de riz".to_string(),
        source: Some("Livre perso".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: riz.clone(),
                quantity: Some(250),
            },
            DishIngredient {
                ingredient: tomate.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: thon.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: mais.clone(),
                quantity: Some(300),
            },
            DishIngredient {
                ingredient: oeuf.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: mayonnaise.clone(),
                quantity: None,
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Pâtes aux lardons".to_string(),
        source: Some("Livre perso".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: lardons.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: onions.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: creme_soja.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: oeuf.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: emmental_rape.clone(),
                quantity: Some(200),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Pâtes aux poireaux et crème".to_string(),
        source: Some("Livre perso".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: poireaux_surgeles.clone(),
                quantity: Some(3),
            },
            DishIngredient {
                ingredient: creme_soja.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: oeuf.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: emmental_rape.clone(),
                quantity: Some(200),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Salade Boulghour & tomates cerises".to_string(),
        source: None,
        elements: Vec::from([
            DishIngredient {
                ingredient: boulghour.clone(),
                quantity: Some(250),
            },
            DishIngredient {
                ingredient: tomates_cerises.clone(),
                quantity: Some(500),
            },
            DishIngredient {
                ingredient: onions.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: chevre_frais.clone(),
                quantity: Some(1),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Salade épinards & oeufs".to_string(),
        source: None,
        elements: Vec::from([
            DishIngredient {
                ingredient: pommes_de_terre.clone(),
                quantity: Some(12),
            },
            DishIngredient {
                ingredient: epinards_surgele.clone(),
                quantity: Some(1000),
            },
            DishIngredient {
                ingredient: emmental_rape.clone(),
                quantity: Some(60),
            },
            DishIngredient {
                ingredient: oeuf.clone(),
                quantity: Some(4),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Salade boulghour & lentilles".to_string(),
        source: Some("Recettes végétariennes, page 44".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: lentilles_vertes.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: boulghour.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: tomates_cerises.clone(),
                quantity: Some(300),
            },
            DishIngredient {
                ingredient: feta.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: jus_de_citron.clone(),
                quantity: None,
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Salade de pâtes grecque".to_string(),
        source: Some("Recettes végétariennes, page 46".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: pates.clone(),
                quantity: Some(500),
            },
            DishIngredient {
                ingredient: onions.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: tomates_cerises.clone(),
                quantity: Some(500),
            },
            DishIngredient {
                ingredient: ail.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: olives_noires.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: feta.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: capres.clone(),
                quantity: None,
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Salade de pâtes aubergine".to_string(),
        source: Some("Recettes végétariennes, page 48".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: pates.clone(),
                quantity: Some(500),
            },
            DishIngredient {
                ingredient: onions.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: ail.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: olives_vertes.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: aubergines.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: capres.clone(),
                quantity: None,
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Salade de pâtes tomates cerises & bleu".to_string(),
        source: Some("Recettes végétariennes, page 49".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: pates.clone(),
                quantity: Some(500),
            },
            DishIngredient {
                ingredient: onions.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: tomates_cerises.clone(),
                quantity: Some(500),
            },
            DishIngredient {
                ingredient: ail.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: olives_noires.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: bleu.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: salade.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: noix.clone(),
                quantity: Some(120),
            },
            DishIngredient {
                ingredient: capres.clone(),
                quantity: None,
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Panzanella".to_string(),
        source: Some("Recettes végétariennes, page 50".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: pain.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: onions.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: tomate.clone(),
                quantity: Some(5),
            },
            DishIngredient {
                ingredient: haricots_blancs.clone(),
                quantity: Some(400),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Salade boulghour & feta".to_string(),
        source: Some("Recettes végétariennes, page 52".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: boulghour.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: onions.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: tomates_cerises.clone(),
                quantity: Some(250),
            },
            DishIngredient {
                ingredient: feta.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: salade.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: jus_de_citron.clone(),
                quantity: None,
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Salade quinoa & céleri".to_string(),
        source: Some("Recettes végétariennes, page 53".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: amandes.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: quinoa.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: ail.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: courgette_fraiche.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: celeri.clone(),
                quantity: Some(3),
            },
            DishIngredient {
                ingredient: jus_de_citron.clone(),
                quantity: None,
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Salade lentilles & chèvre".to_string(),
        source: Some("Recettes végétariennes, page 54".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: lentilles_vertes.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: echalottes.clone(),
                quantity: Some(3),
            },
            DishIngredient {
                ingredient: carotte_fraiche.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: epinards_surgele.clone(),
                quantity: Some(150),
            },
            DishIngredient {
                ingredient: celeri.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: chevre_frais.clone(),
                quantity: Some(1),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Bucatini sauce tomate & amandes".to_string(),
        source: Some("Recettes végétariennes, page 57".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: pain.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: onions.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: tomate.clone(),
                quantity: Some(7),
            },
            DishIngredient {
                ingredient: amandes.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: pates.clone(),
                quantity: Some(500),
            },
            DishIngredient {
                ingredient: emmental_rape.clone(),
                quantity: Some(120),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Pates tomates cerises & chèvre".to_string(),
        source: Some("Recettes végétariennes, page 59".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: pates.clone(),
                quantity: Some(500),
            },
            DishIngredient {
                ingredient: ail.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: tomates_cerises.clone(),
                quantity: Some(750),
            },
            DishIngredient {
                ingredient: olives_noires.clone(),
                quantity: Some(100),
            },
            DishIngredient {
                ingredient: chevre_frais.clone(),
                quantity: Some(1),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Pates au choux-fleur".to_string(),
        source: Some("Recettes végétariennes, page 62".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: pates.clone(),
                quantity: Some(500),
            },
            DishIngredient {
                ingredient: ail.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: chou_fleur_surgele.clone(),
                quantity: Some(700),
            },
            DishIngredient {
                ingredient: emmental_rape.clone(),
                quantity: Some(60),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Pates alla caprese".to_string(),
        source: Some("Recettes végétariennes, page 62".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: pates.clone(),
                quantity: Some(500),
            },
            DishIngredient {
                ingredient: tomates_cerises.clone(),
                quantity: Some(750),
            },
            DishIngredient {
                ingredient: mozzarella.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: ail.clone(),
                quantity: Some(2),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Pates aux amandes".to_string(),
        source: Some("Recettes végétariennes, page 62".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: pates.clone(),
                quantity: Some(500),
            },
            DishIngredient {
                ingredient: tomate.clone(),
                quantity: Some(3),
            },
            DishIngredient {
                ingredient: amandes.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: ail.clone(),
                quantity: Some(2),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Pates épicées aux tomates".to_string(),
        source: Some("Recettes végétariennes, page 62".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: pates.clone(),
                quantity: Some(500),
            },
            DishIngredient {
                ingredient: tomate.clone(),
                quantity: Some(6),
            },
            DishIngredient {
                ingredient: emmental_rape.clone(),
                quantity: Some(60),
            },
            DishIngredient {
                ingredient: olives_noires.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: ail.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: piment_rouge.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: capres.clone(),
                quantity: None,
            },
            DishIngredient {
                ingredient: concentre_tomates.clone(),
                quantity: None,
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Gratin de macaronni aux petits poids".to_string(),
        source: Some("Recettes végétariennes, page 64".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: pates.clone(),
                quantity: Some(500),
            },
            DishIngredient {
                ingredient: petits_pois.clone(),
                quantity: Some(300),
            },
            DishIngredient {
                ingredient: beurre.clone(),
                quantity: Some(105),
            },
            DishIngredient {
                ingredient: emmental_rape.clone(),
                quantity: Some(150),
            },
            DishIngredient {
                ingredient: mozzarella.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: lait.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: chapelure.clone(),
                quantity: None,
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Pâtes au brocoli".to_string(),
        source: Some("Recettes végétariennes, page 67".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: pates.clone(),
                quantity: Some(500),
            },
            DishIngredient {
                ingredient: brocolis.clone(),
                quantity: Some(400),
            },
            DishIngredient {
                ingredient: ail.clone(),
                quantity: Some(3),
            },
            DishIngredient {
                ingredient: piment.clone(),
                quantity: None,
            },
            DishIngredient {
                ingredient: tomates_cerises.clone(),
                quantity: Some(300),
            },
            DishIngredient {
                ingredient: parmesan.clone(),
                quantity: Some(120),
            },
        ]),
    });

    cook_book.push(Dish {
        name: "Risotto bleu & poires".to_string(),
        source: Some("Recettes végétariennes, page 72".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: riz.clone(),
                quantity: Some(500),
            },
            DishIngredient {
                ingredient: onions.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: beurre.clone(),
                quantity: Some(90),
            },
            DishIngredient {
                ingredient: poire.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: noix.clone(),
                quantity: Some(60),
            },
            DishIngredient {
                ingredient: persil.clone(),
                quantity: None,
            },
            DishIngredient {
                ingredient: bouillon_legumes.clone(),
                quantity: None,
            },
            DishIngredient {
                ingredient: cognac.clone(),
                quantity: None,
            },
        ]),
    });

    // cook_book.push(Dish {
    //     name: "Curry épicé à l'ananas".to_string(),
    //     source: Some("Recettes végétariennes, page 82".to_string()),
    //     elements: Vec::from([
    //         DishIngredient {
    //             ingredient: tomate.clone(),
    //             quantity: Some(2),
    //         },
    //         DishIngredient {
    //             ingredient: ananas.clone(),
    //             quantity: Some(225),
    //         },
    //     ]),
    // });

    cook_book.push(Dish {
        name: "Curry tofu & épinards".to_string(),
        source: Some("Recettes végétariennes, page 84".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: tofu.clone(),
                quantity: Some(400),
            },
            DishIngredient {
                ingredient: onions.clone(),
                quantity: Some(2),
            },
            DishIngredient {
                ingredient: tomate.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: ail.clone(),
                quantity: Some(5),
            },
            DishIngredient {
                ingredient: epinards_surgele.clone(),
                quantity: Some(200),
            },
            DishIngredient {
                ingredient: huile_arachide.clone(),
                quantity: None,
            },
            DishIngredient {
                ingredient: curcuma.clone(),
                quantity: None,
            },
            DishIngredient {
                ingredient: cumin.clone(),
                quantity: None,
            },
        ]),
    });

    // cook_book.push(Dish {
    //     name: "Nouilles épicées au tofu".to_string(),
    //     source: Some("Recettes végétariennes, page 85".to_string()),
    //     elements: Vec::from([
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //     ]),
    // });

    // cook_book.push(Dish {
    //     name: "Crêpes ricotta & courgettes".to_string(),
    //     source: Some("Recettes végétariennes, page 86".to_string()),
    //     elements: Vec::from([
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //     ]),
    // });

    // cook_book.push(Dish {
    //     name: "Flan asperges & courgette".to_string(),
    //     source: Some("Recettes végétariennes, page 88".to_string()),
    //     elements: Vec::from([
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //     ]),
    // });

    // cook_book.push(Dish {
    //     name: "Cake asperges & olives".to_string(),
    //     source: Some("Recettes végétariennes, page 89".to_string()),
    //     elements: Vec::from([
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //     ]),
    // });

    // cook_book.push(Dish {
    //     name: "Mijoté d'aubergines".to_string(),
    //     source: Some("Recettes végétariennes, page 90".to_string()),
    //     elements: Vec::from([
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //     ]),
    // });

    // cook_book.push(Dish {
    //     name: "Caponata au riz".to_string(),
    //     source: Some("Recettes végétariennes, page 90".to_string()),
    //     elements: Vec::from([
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //     ]),
    // });

    // cook_book.push(Dish {
    //     name: "Chili aux haricots noirs".to_string(),
    //     source: Some("Recettes végétariennes, page 90".to_string()),
    //     elements: Vec::from([
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //     ]),
    // });

    cook_book.push(Dish {
        name: "Chou-fleur aux tomates".to_string(),
        source: Some("Recettes végétariennes, page 90".to_string()),
        elements: Vec::from([
            DishIngredient {
                ingredient: chou_fleur_frais.clone(),
                quantity: Some(1),
            },
            DishIngredient {
                ingredient: ail.clone(),
                quantity: Some(4),
            },
            DishIngredient {
                ingredient: tomate.clone(),
                quantity: Some(10),
            },
            DishIngredient {
                ingredient: persil.clone(),
                quantity: None,
            },
        ]),
    });

    // cook_book.push(Dish {
    //     name: "Gratin de pois chiches".to_string(),
    //     source: Some("Recettes végétariennes, page 96".to_string()),
    //     elements: Vec::from([
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //     ]),
    // });

    // cook_book.push(Dish {
    //     name: "Lasagnes éinards & ricotta".to_string(),
    //     source: Some("Recettes végétariennes, page 97".to_string()),
    //     elements: Vec::from([
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //     ]),
    // });

    // cook_book.push(Dish {
    //     name: "Gratin de patates douces".to_string(),
    //     source: Some("Recettes végétariennes, page 98".to_string()),
    //     elements: Vec::from([
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //     ]),
    // });

    // cook_book.push(Dish {
    //     name: "Gratin de choux-fleur".to_string(),
    //     source: Some("Recettes végétariennes, page 102".to_string()),
    //     elements: Vec::from([
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //     ]),
    // });

    // cook_book.push(Dish {
    //     name: "Parmigiana d'aubergines".to_string(),
    //     source: Some("Recettes végétariennes, page 103".to_string()),
    //     elements: Vec::from([
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //         DishIngredient {
    //             ingredient: pates.clone(),
    //             quantity: Some(500),
    //         },
    //     ]),
    // });

    return cook_book;
}

use array_tool::vec::Uniq;
use rand::seq::SliceRandom;
use std::{collections::HashMap, io::Write};

use crate::data::*;

pub fn recipe_selection(cook_book: Vec<Dish>) -> Vec<Dish> {
    let mut selection = Vec::<Dish>::new();

    let mut selecting = true;
    while selecting {
        println!("");
        println!("Selection des recettes:   (pour 4 personnes)");
        println!("");
        println!("[0]: Terminé");
        println!("[1]: Aléatoire");
        for (i, recipe) in cook_book.iter().enumerate() {
            print!("[{}]: {}", i + 2, recipe.name);
            let already_present = selection.iter().filter(|a| *a == recipe).count();
            if already_present > 0 {
                print!("    --- Sélectionné ---");
                if already_present > 1 {
                    print!("    [{}x]", already_present);
                }
            }
            println!("");
        }

        match prompt() {
            Some(id_selected) => {
                if id_selected == 0 {
                    selecting = false;
                } else if id_selected == 1 {
                    match random_recipe(cook_book.clone(), selection.clone()) {
                        Some(r) => selection.push(r),
                        None => println!("Could not get a random recipe."),
                    };
                } else {
                    match cook_book.get(id_selected - 2) {
                        Some(r) => selection.push(r.clone()),
                        None => println!("Indice de repas non reconnu."),
                    }
                }
            }
            None => {
                println!("Indice de repas non reconnu.");
            }
        }
    }
    return selection;
}

pub fn random_recipe(cook_book: Vec<Dish>, selection: Vec<Dish>) -> Option<Dish> {
    let remaining: Vec<Dish> = cook_book
        .into_iter()
        .filter(|a| !selection.iter().any(|b| a == b))
        .collect();

    match remaining.choose(&mut rand::thread_rng()) {
        Some(d) => Some(d.clone()),
        None => None,
    }
}

pub fn prompt() -> Option<usize> {
    let mut line = String::new();
    print!("> ");
    std::io::stdout().flush().unwrap();
    std::io::stdin()
        .read_line(&mut line)
        .expect("Error: Could not read a line");

    return match line.trim().to_string().parse::<usize>() {
        Ok(r) => Some(r),
        Err(_) => None,
    };
}

pub fn sort_selection(selection: Vec<Dish>) -> HashMap<Category, HashMap<Ingredient, usize>> {
    let mut sorted_buying_list = HashMap::<Category, HashMap<Ingredient, usize>>::new();
    sorted_buying_list.insert(Category::Légumes, HashMap::new());
    sorted_buying_list.insert(Category::Fruits, HashMap::new());
    sorted_buying_list.insert(Category::Frais, HashMap::new());
    sorted_buying_list.insert(Category::Viandes, HashMap::new());
    sorted_buying_list.insert(Category::FromagesPain, HashMap::new());
    sorted_buying_list.insert(Category::Conserves, HashMap::new());
    sorted_buying_list.insert(Category::Monde, HashMap::new());
    sorted_buying_list.insert(Category::Féculents, HashMap::new());
    sorted_buying_list.insert(Category::Déjeuner, HashMap::new());
    sorted_buying_list.insert(Category::CrêmeLaitOeufs, HashMap::new());
    sorted_buying_list.insert(Category::AlcoolApéritif, HashMap::new());
    sorted_buying_list.insert(Category::MénageEntretienPQ, HashMap::new());
    sorted_buying_list.insert(Category::Chats, HashMap::new());
    sorted_buying_list.insert(Category::Bio, HashMap::new());
    sorted_buying_list.insert(Category::Surgelé, HashMap::new());
    sorted_buying_list.insert(Category::Autre, HashMap::new());

    // Reversing ingredients sorting
    for recipe in selection {
        for element in recipe.elements {
            let quantity = match element.quantity {
                Some(q) => q,
                None => 0,
            };
            let sublist = sorted_buying_list
                .get_mut(&element.ingredient.category)
                .unwrap();
            match sublist.get_mut(&element.ingredient) {
                Some(value) => {
                    *value = *value + quantity;
                }
                None => {
                    sublist.insert(element.ingredient, quantity);
                }
            }
        }
    }

    return sorted_buying_list;
}

pub fn display_dish_selection(selection: Vec<Dish>) {
    let mut dish_counts = HashMap::<Dish, usize>::new();
    let uniques = selection.unique();
    for dish in uniques.iter() {
        dish_counts.insert(
            dish.clone(),
            selection.iter().filter(|d| *d == dish).count(),
        );
    }

    println!("");
    println!("===== Liste des recettes =====");
    for (dish, count) in dish_counts {
        print!(" - {}", dish.name);
        if count > 1 {
            print!("  [{}x]", count);
        }
        println!("");
    }
}

pub fn display_buying_list(sorted_buying_list: HashMap<Category, HashMap<Ingredient, usize>>) {
    println!("");
    println!("===== Liste de courses =====");
    display_sublist(&sorted_buying_list, Category::Légumes);
    display_sublist(&sorted_buying_list, Category::Fruits);
    display_sublist(&sorted_buying_list, Category::Frais);
    display_sublist(&sorted_buying_list, Category::Viandes);
    display_sublist(&sorted_buying_list, Category::FromagesPain);
    display_sublist(&sorted_buying_list, Category::Conserves);
    display_sublist(&sorted_buying_list, Category::Monde);
    display_sublist(&sorted_buying_list, Category::Féculents);
    display_sublist(&sorted_buying_list, Category::Déjeuner);
    display_sublist(&sorted_buying_list, Category::CrêmeLaitOeufs);
    display_sublist(&sorted_buying_list, Category::AlcoolApéritif);
    display_sublist(&sorted_buying_list, Category::MénageEntretienPQ);
    display_sublist(&sorted_buying_list, Category::Chats);
    display_sublist(&sorted_buying_list, Category::Bio);
    display_sublist(&sorted_buying_list, Category::Surgelé);
    display_sublist(&sorted_buying_list, Category::Autre);
}

pub fn display_sublist(
    sorted_buying_list: &HashMap<Category, HashMap<Ingredient, usize>>,
    cat: Category,
) {
    if let Some(sublist) = sorted_buying_list.get(&cat) {
        if sublist.len() > 0 {
            println!("");
            println!("--- {:#?} ---", cat);
            for (ingredient, quantity) in sublist.iter() {
                let quantity = match quantity {
                    0 => "".to_string(),
                    _ => quantity.to_string(),
                };

                println!("  {}: {}{}", ingredient.name, quantity, ingredient.unit);
            }
        }
    }
}

pub fn display_details(selection: Vec<Dish>) {
    println!("");
    println!("===== Details =====");
    for recipe in selection.unique() {
        println!("");
        println!("{}:", recipe.name);
        if let Some(source) = recipe.source {
            println!("{}", source);
        }
        for element in recipe.elements {
            let formated_quantity = match element.quantity {
                Some(q) => format!(
                    "{}: {}{}",
                    element.ingredient.name, q, element.ingredient.unit
                ),
                None => format!("un peu de {}", element.ingredient.name),
            };

            println!("  - {}", formated_quantity);
        }
    }
    println!("");
}

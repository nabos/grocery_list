mod data;
use data::*;

mod ui;
use ui::*;

fn main() {
    let cook_book = build_cook_book();

    let selection = recipe_selection(cook_book);

    let sorted_buying_list = sort_selection(selection.clone());

    println!("=====================================================");
    display_dish_selection(selection.clone());
    display_buying_list(sorted_buying_list);
    display_details(selection);
}
